export default [
  {
    'positive': 'Sim',
    'negative': 'Não'
  },
  {
    'positive': 'A Favor',
    'negative': 'Contra'
  },
  {
    'positive': 'Gosto',
    'negative': 'Não gosto'
  },
  {
    'positive': 'Curto',
    'negative': 'Não curto'
  },
  {
    'positive': 'Voto',
    'negative': 'Não voto'
  },
  {
    'positive': 'Acredito',
    'negative': 'Não acredito'
  },
  {
    'positive': 'Apoio',
    'negative': 'Não apoio'
  },
  {
    'positive': 'Concordo',
    'negative': 'Discordo'
  }
]
