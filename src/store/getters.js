export default {
  getUser: state => state.user,
  getDeck: state => state.deck,
  getReactions: state => state.reactions,
  getCards: state => state.arguments
}
